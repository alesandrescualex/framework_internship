/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Btn.c
 *    \author     Alesandrescu Ionut Alexandru
 *    \brief      Declares all the link-time and / or post-build configuration parameters of the software component.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Btn.h"
#include "IoHwAb.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
static Btn_Specif Btn_array[3];


/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the active values to be used for processing the digital input ECU signals. */

/** \brief  Defines the correlation between the instance IDs and the IoHwAb channel IDs. */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

void Btn_Init(void)
{
   Btn_array[0].btn_name = 'l';
   Btn_array[0].State = BTN_NOT_PRESSED;
   Btn_array[0].debounce_counter = 0;
   Btn_array[0].btn_pin = IOHWAB_DIGITAL_CHANNEL_BTN_LEFT;

   Btn_array[1].btn_name = 'r';
   Btn_array[1].State = BTN_NOT_PRESSED;
   Btn_array[1].debounce_counter = 0;
   Btn_array[1].btn_pin = IOHWAB_DIGITAL_CHANNEL_BTN_RIGHT;

   Btn_array[2].btn_name = 'h';
   Btn_array[2].State = BTN_NOT_PRESSED;
   Btn_array[2].debounce_counter = 0;
   Btn_array[2].btn_pin = IOHWAB_DIGITAL_CHANNEL_BTN_HAZARD;

}

void Btn_MainFunction(void)
{
   uint8 i;
   for (i = 0; i < 3; i++)
   {
      if (Btn_array[i].State == BTN_NOT_PRESSED)
      {
         if (STD_HIGH == IoHwAb_DigitalGetChannel(Btn_array[i].btn_pin))
         {

            Btn_array[i].debounce_counter++;
         }
         else
         {
            Btn_array[i].debounce_counter = 0;
         }

         if (Btn_array[i].debounce_counter > 4)
         {
            Btn_array[i].State = BTN_PRESSED;

            Btn_array[i].debounce_counter = 0;

         }

      }
      if (Btn_array[i].State == BTN_PRESSED)
      {
         if (STD_LOW == IoHwAb_DigitalGetChannel(Btn_array[i].btn_pin))
            Btn_array[i].debounce_counter++;
         else
            Btn_array[i].debounce_counter = 0;
         if (Btn_array[i].debounce_counter > 4)
         {
            Btn_array[i].State = BTN_NOT_PRESSED;

            Btn_array[i].debounce_counter = 0;

         }

      }
   }
}


   /*if(Counter >= 5)
    {
    BTN_PRESSED;
    }
    else
    {
    BTN_NOT_PRESSED;
    }*/

   /* IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_CHANNEL_BTN_RIGHT);
    IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_CHANNEL_BTN_LEFT);
    IoHwAb_DigitalGetChannel(IOHWAB_DIGITAL_CHANNEL_BTN_HAZARD);*/



Btn_PressStateType Btn_GetState(uint8 uc_Id)
{
   return Btn_array[uc_Id].State;
}

