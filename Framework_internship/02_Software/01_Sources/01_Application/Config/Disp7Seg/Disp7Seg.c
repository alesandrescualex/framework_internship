/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Disp7Seg.c
 *    \author     Alesandrescu Ionut Alexandru
 *    \brief
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "IoHwAb.h"
#include "Disp7Seg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

static Disp_Specif Disp_array[2];
uint8 Left_Digit = -1;
uint8 Right_Digit = -1;
uint8 DotsState;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

void LeftDigit(uint8 rd)
{
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_LOW);
   if (rd == 0)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

   }

   else if (rd == 1)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

   }

   else if (rd == 2)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
   }
   else if (rd == 3)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
   }
   else if (rd == 4)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
   }
   else if (rd == 5)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
   }
   else if (rd == 6)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);

   }

   else if (rd == 7)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

   }
   else if (rd == 8)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
   }

   else if (rd == 9)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
   }
   else
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);

   }
}

void RightDigit(uint8 ld)
{
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_LOW);
   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_LOW);
   if (ld == 0)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);

   }

   else if (ld == 1)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);

   }

   else if (ld == 2)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
   }
   else if (ld == 3)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
   }
   else if (ld == 4)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
   }
   else if (ld == 5)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
   }
   else if (ld == 6)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);

   }

   else if (ld == 7)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);

      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);

   }
   else if (ld == 8)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
   }

   else if (ld == 9)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
   }
   else
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
   }
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

void Disp7Seg_Init()
{
   Disp_array[0].disp_pin = IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1;
   Disp_array[0].disp_duty = STD_LOW;

   Disp_array[1].disp_pin = IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2;
   Disp_array[1].disp_duty = STD_LOW;

   IoHwAb_DigitalSetChannel(Disp_array[DISP7SEG_INSTANCE_LEFT].disp_pin,
                            Disp_array[DISP7SEG_INSTANCE_LEFT].disp_duty);

   IoHwAb_DigitalSetChannel(Disp_array[DISP7SEG_INSTANCE_RIGHT].disp_pin,
                            Disp_array[DISP7SEG_INSTANCE_RIGHT].disp_duty);
}

void Disp7Seg_MainFunction()
{
   LeftDigit(Left_Digit);
   RightDigit(Right_Digit);

   if (DotsState == DISP7SEG_BOTH_OFF)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (DotsState == DISP7SEG_BOTH_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }
   else if (DotsState == DISP7SEG_LEFT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (DotsState == DISP7SEG_RIGHT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }

}

void Disp7Seg_SetState(uint8 uc_Id, uint8 uc_DecValue, Disp7Seg_DotsType t_Dots)
{
   uc_Id++;
   DotsState = t_Dots;

   if (uc_DecValue < 100)
   {
      Right_Digit = uc_DecValue % 10;
      Left_Digit = uc_DecValue / 10;
   }

   else if (uc_DecValue == 255)
   {
      Left_Digit = 255;
      Right_Digit = 255;
   }

}
