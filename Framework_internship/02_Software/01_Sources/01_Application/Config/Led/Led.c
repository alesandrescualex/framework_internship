/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       SwcExample_Cfg.c
 *    \author     Alesandrescu Ionut Alexandru
 *    \brief
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Led.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
   static Led_Specif Led_array[4];
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
   uint16 Led_Interpol(uint16 Duty)
{
   uint16 Dutymax = 10000;
   uint16 Dutymin = 0;
   uint16 OutMax = 0x8000;
   uint16 OutMin = 0;

   uint16 FinalResult = (((uint32) OutMax * (Duty - Dutymin)) + OutMin * (Duty-Dutymax )) / (Dutymax - Dutymin);

   return FinalResult;
}
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

 void Led_Init(void)
   {
     Led_array[LED_FRONT_RIGHT].led_duty = 0;
     Led_array[LED_FRONT_RIGHT].led_pin = IOHWAB_PWM_LED_FRONT_RIGHT;

     Led_array[LED_FRONT_LEFT].led_duty = 0;
     Led_array[LED_FRONT_LEFT].led_pin = IOHWAB_PWM_LED_FRONT_LEFT;

     Led_array[LED_BACK_RIGHT].led_duty = 0;
     Led_array[LED_BACK_RIGHT].led_pin = IOHWAB_PWM_LED_BACK_RIGHT;

     Led_array[LED_BACK_LEFT].led_duty = 0;
     Led_array[LED_BACK_LEFT].led_pin = IOHWAB_PWM_LED_BACK_LEFT;


      IoHwAb_PwmSetChannel(Led_array[LED_FRONT_RIGHT].led_pin, Led_array[LED_FRONT_RIGHT].led_duty = 0);
      IoHwAb_PwmSetChannel(Led_array[LED_FRONT_LEFT].led_pin, Led_array[LED_FRONT_LEFT].led_duty);
      IoHwAb_PwmSetChannel(Led_array[LED_BACK_RIGHT].led_pin = IOHWAB_PWM_LED_BACK_RIGHT,
                           Led_array[LED_BACK_RIGHT].led_duty);
      IoHwAb_PwmSetChannel( Led_array[LED_BACK_LEFT].led_pin , Led_array[LED_BACK_LEFT].led_duty );
   }

  void Led_MainFunction(void)
   {

      uint8 i = 0;
      for(i=0; i<4; i++)
      {
         IoHwAb_PwmSetChannel(Led_array[i].led_pin, Led_array[i].led_duty);
      }

   }

   void Led_SetState(uint8 uc_Id, Led_DutyType t_Duty)
   {
      static uint16 interpol_res;

         if (t_Duty <= 10000)
      {
            interpol_res = Led_Interpol(t_Duty);
            Led_array[uc_Id].led_duty = interpol_res;
      }

   }
