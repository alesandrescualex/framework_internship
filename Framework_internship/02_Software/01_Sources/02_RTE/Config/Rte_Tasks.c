/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Tasks.c
 *    \author     Alesandrescu Ionut Alexandru
 *    \brief      Implements all the OS tasks. Maps software component runnables to tasks.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Os.h"
#include "IoHwAb.h"
#include "Btn.h"
#include "PotMet.h"
#include "Led.h"
#include "Disp7Seg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Specifies the upper duty cycle limit to be used in the LEDs dimming. */
#define RTE_DIMMING_DUTY_UPPER_LIMIT           (0x2000UL)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the dimming states of the LEDs. */
typedef enum
{
   RTE_UPWARDS,
   RTE_DOWNWARDS,
} Rte_DimmingType;

typedef enum
{
   RTE_STATE_MACHINE_IDLE,
   RTE_STATE_MACHINE_LEFT_FLASHING,
   RTE_STATE_MACHINE_RIGHT_FLASHING,
   RTE_STATE_MACHINE_HAZARD_FLASHING,
   RTE_STATE_MACHINE_CONFIG1,
   RTE_STATE_MACHINE_CONFIG2,
   RTE_STATE_MACHINE_CONFIG3,
   RTE_STATE_MACHINE_CONFIG4
} Rte_StateMachineType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
uint8 Led_counter;

///** \brief  Specifies the number of executed 100 ms tasks for time tracking purposes. */
//static uint8 Rte_uc_100MsTaskCounter = 0U;
//
///** \brief  Specifies the ID of LED that is influenced by the up-down dimming. */
//static uint8 Rte_uc_CurrentDimmingLed = 0U;
//
///** \brief  Specifies the current duty cycle to be applied on the current dimming influenced LED. */
//static uint32 Rte_ul_CurrentDuty = 0UL;
//
///** \brief  Specifies the current dimming state of the dimming influenced LED. */
//static Rte_DimmingType Rte_t_DimmingState = RTE_UPWARDS;
//
///** \brief  Specifies the current potentiometer reading. */
//static IoHwAb_AnalogLevelType Rte_t_PotentiometerValue;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
uint8 Counter_right = 0;
uint8 Counter_left = 0;
uint16 Counter_flash = 0;
uint8 Counter = 0;
uint8 Button_std_left = 0;
uint8 Button_std_right = 0;
uint8 Counter_digit = 4;
uint8 Button_std_hazard = 0;
uint8 Hazard_Counter = 0;
uint8 numberOfFlashes;
uint8 Counter_Right = 0;
uint8 Counter_digit_right = 4;
uint16 Counter_flash_right = 0;
uint8 Counter_H = 0;
uint16 CounterSwitchStates = 0;
uint8 left_flag = 0;
uint8 right_flag = 0;
uint16 counterConfig = 0;
uint16 counterHazardms = 0;

Btn_PressStateType Rte_gt_LeftLastState = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_RightLastState = BTN_NOT_PRESSED;
Btn_PressStateType Rte_gt_HazardLastState = BTN_NOT_PRESSED;
Rte_StateMachineType Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
Btn_PressStateType t_LeftCurrentState;
Btn_PressStateType t_RightCurrentState;
Btn_PressStateType t_HazardCurrentState;

uint8 Counter_Hazard = 0;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

void Idle_State()
{
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, 255, DISP7SEG_BOTH_ON);
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
}

void leftSignalign()
{

   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, numberOfFlashes, DISP7SEG_LEFT_ON);
   Counter++;
   if (Counter < 50)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 9000);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 9000);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   }
   else if (Counter < 200)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   }
   else
   {
      numberOfFlashes--;
      Counter = 0;
   }

   if (numberOfFlashes < 1)
   {
      Idle_State();
      numberOfFlashes = 4;
      Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
   }
}

void rightSignalign()
{

   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, numberOfFlashes, DISP7SEG_RIGHT_ON);
   Counter_Right++;
   if (Counter_Right < 50)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 9000);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 9000);
   }
   else if (Counter_Right < 200)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   }
   else
   {
      numberOfFlashes--;
      Counter_Right = 0;
   }

   if (numberOfFlashes < 1)
   {
      Idle_State();
      numberOfFlashes = 4;
      Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
   }
}

void LeftTurnFinal()
{
   if(Btn_GetState(BTN_INSTANCE_LEFT) == BTN_PRESSED)
   {
      left_flag = 1;
   }

   if(Btn_GetState(BTN_INSTANCE_LEFT) == BTN_NOT_PRESSED && (left_flag = 1))
   {
      leftSignalign();
   }
}


void RightTurnFinal()
{
   if(Btn_GetState(BTN_INSTANCE_RIGHT) == BTN_PRESSED)
   {
      right_flag = 1;
   }

   if(Btn_GetState(BTN_INSTANCE_RIGHT) == BTN_NOT_PRESSED && (right_flag = 1))
   {
      rightSignalign();
   }
}



void Hazard_State()
{

   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, 255, DISP7SEG_BOTH_OFF);

   Hazard_Counter++;
   if (Hazard_Counter <= 50)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 9000);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 9000);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 9000);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 9000);

   }
   else if (Hazard_Counter > 50 && Hazard_Counter < 200)
   {
      Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
      Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
      Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);
   }
   else
   {
      Hazard_Counter = 0;
   }

}

uint16 Interpolare()
{
   PotMet_PositionType result;
   result = PotMet_GetState(POTMET_ANALOG_POT1);

   uint16 output;

   uint16 input_min = 0;
   uint16 input_max = 10000;
   uint16 output_min = 10;
   uint16 output_max = 90;

   output = ((uint32) (output_max * (result - input_min)) + output_min * (result - input_max))
      / (input_max - input_min);

   return output;

}

void Config_One()
{
   Led_SetState(IOHWAB_PWM_LED_FRONT_LEFT, 9000);
   Led_SetState(IOHWAB_PWM_LED_BACK_LEFT, 0);
   Led_SetState(IOHWAB_PWM_LED_FRONT_RIGHT, 0);
   Led_SetState(IOHWAB_PWM_LED_BACK_RIGHT, 0);

   for (uint8 i = 0; i < 5; i++)
   {
      Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, 1, DISP7SEG_BOTH_OFF);
   }
   uint16 resul_interpolare_Config = Interpolare();
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY, resul_interpolare_Config, DISP7SEG_BOTH_OFF);

}

void Config_Two()
{

}

void Config_Three()
{

}

void Config_Four()
{

}

void NormalState()
{
   t_LeftCurrentState = Btn_GetState(BTN_INSTANCE_LEFT);
   t_RightCurrentState = Btn_GetState(BTN_INSTANCE_RIGHT);
   t_HazardCurrentState = Btn_GetState(BTN_INSTANCE_HAZARD);

   if ((Rte_gt_LeftLastState == BTN_NOT_PRESSED) && (t_LeftCurrentState == BTN_PRESSED))
   {
      if ((Rte_gt_CurrentStateMachine != RTE_STATE_MACHINE_HAZARD_FLASHING))
      {
         numberOfFlashes = 4;
         Counter = 0;
         if (Rte_gt_CurrentStateMachine <= RTE_STATE_MACHINE_HAZARD_FLASHING)
         {
            Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_LEFT_FLASHING;
         }
      }
   }

   if ((Rte_gt_RightLastState == BTN_NOT_PRESSED) && (t_RightCurrentState == BTN_PRESSED))
   {
      if ((Rte_gt_CurrentStateMachine != RTE_STATE_MACHINE_HAZARD_FLASHING))
      {
         Counter_Right = 0;
         numberOfFlashes = 4;
         if (Rte_gt_CurrentStateMachine <= RTE_STATE_MACHINE_HAZARD_FLASHING)
         {
            Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_RIGHT_FLASHING;
         }
      }
   }

   if (t_HazardCurrentState == BTN_PRESSED)
   {
      if (Counter_H == 40)
      {
         if (Rte_gt_CurrentStateMachine != RTE_STATE_MACHINE_HAZARD_FLASHING)
         {
            Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_HAZARD_FLASHING;
         }
         else
         {
            Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
         }
         Counter_H++;
      }
      else if (Counter_H < 40)
      {
         Counter_H++;
      }
      else
      {

      }
   }
   else
   {
      Counter_H = 0;
   }



   if ((t_LeftCurrentState == BTN_PRESSED) && (t_RightCurrentState == BTN_PRESSED))
      {

         if (counterConfig == 400U)
         {
            if (Rte_gt_CurrentStateMachine != RTE_STATE_MACHINE_CONFIG1)
            {
               Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_CONFIG1;
            }
            else
            {
               Rte_gt_CurrentStateMachine = RTE_STATE_MACHINE_IDLE;
            }
            counterConfig++;
         }
         else if (counterHazardms < 400U)
         {
            counterConfig++;
         }
         else
         {
         }
      }
      else
      {
         counterConfig = 0U;
      }





   if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_HAZARD_FLASHING)
   {
      Hazard_State();
   }
   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_RIGHT_FLASHING)
   {
      RightTurnFinal();
   }
   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_LEFT_FLASHING)
   {
      LeftTurnFinal();
   }
   else if (Rte_gt_CurrentStateMachine == RTE_STATE_MACHINE_CONFIG1)
   {
      Config_One();
   }
   else
   {
      Idle_State();
   }






   Rte_gt_LeftLastState = t_LeftCurrentState;
   Rte_gt_RightLastState = t_RightCurrentState;
   Rte_gt_HazardLastState = t_HazardCurrentState;

}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes RTE and all the SWCs.
 * \param      -
 * \return     -
 */
TASK(OS_INIT_TASK)
{
//   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
//   IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
   Btn_Init();
   PotMet_Init();
   Led_Init();
   Disp7Seg_Init();

   Idle_State();
}

/**
 * \brief      Application specific 500 us periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_FAST_TASK)
{
}

/**
 * \brief      Application specific 5 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_5MS_TASK)
{

   NormalState();

   Btn_MainFunction();
   Led_MainFunction();
   PotMet_MainFunction();
   Disp7Seg_MainFunction();

}

/**
 * \brief      Application specific 10 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_10MS_TASK)
{

}

/**
 * \brief      Application specific 20 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_20MS_TASK)
{

}

/**
 * \brief      Application specific 100 ms periodicity task.
 * \param     -
 * \return    -
 */
TASK(OS_100MS_TASK)
{

}

/**
 * \brief      Application background task.
 * \param      -
 * \return     -
 */
TASK(OS_BACKGROUND_TASK)
{
}

