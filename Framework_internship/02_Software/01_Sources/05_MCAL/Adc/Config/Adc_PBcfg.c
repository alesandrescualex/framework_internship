/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc_PBcfg.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Sets up the AUTOSAR ADC initialization and groups configurations, both being included in the global
 *                exported configuration.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Adc.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the total number of registers to be loaded in the initialization function. */
#define ADC_NUMBER_OF_INIT_REGISTERS      (5U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the configuration of all the groups.
 *
 * Each group is defined by the base address of the ADC hardware unit and the ID of the ADC hardware channel to be
 * used in the regular sequence control register. */
static const Adc_GroupConfigType Adc_kat_GroupsConfig[ADC_NUMBER_OF_GROUPS] =
{
   { ADC1, (ADC_SQR3_SQ1_3 | ADC_SQR3_SQ1_2 | ADC_SQR3_SQ1_1 | ADC_SQR3_SQ1_0) },
};

/** \brief  Stores the configuration of all the registers to be set in the ADC initialization function. */
static const RegInit_Masked32BitsSingleType Adc_kat_InitLoadRegisters[ADC_NUMBER_OF_INIT_REGISTERS] =
{
   /* Set the resolution of conversions to 12 bits. (RES = b00). */
   { &ADC1->CR1, (uint32) ~ADC_CR1_RES, 0UL },

   /* Set the alignment of conversions to right. (ALIGN = b0). */
   { &ADC1->CR2, (uint32) ~ADC_CR2_ALIGN, 0UL },

   /* Set channel sequence length to 1 conversion (L = b0000). */
   { &ADC1->SQR1, (uint32) ~ADC_SQR1_L, 0UL },

   /* Set channel 8 sampling time to 84 cycles (SMP8 = b100). */
   { &ADC1->SMPR2, (uint32) ~ADC_SMPR2_SMP8, ADC_SMPR2_SMP8_2 },

   /* Enable ADC1 hardware unit (ADON = b1). */
   { &ADC1->CR2, (uint32) ~ADC_CR2_ADON, ADC_CR2_ADON },
};

/** \brief  References the configuration of all the registers to be set in the ADC initialization function and the
 *          number of registers to be initialized. */
static const RegInit_Masked32BitsConfigType Adc_kt_InitConfig =
{ Adc_kat_InitLoadRegisters, ADC_NUMBER_OF_INIT_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the post-build configuration. References the initialization configuration and the ADC groups
 *          configuration. */
const Adc_ConfigType Adc_gkt_Config =
{
   &Adc_kt_InitConfig,
   Adc_kat_GroupsConfig,
};

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
