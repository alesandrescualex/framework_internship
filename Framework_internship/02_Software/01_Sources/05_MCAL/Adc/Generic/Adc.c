/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements the AUTOSAR ADC initialization function and a minimal set of interfaces for reading one
 *                ADC channel at a time. In this implementation each ADC groups consists of (exactly) one ADC channel.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Adc.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Stores the start addresses of the result buffers for all ADC groups. */
static Adc_ValueGroupType* Adc_apt_ResultBuffersAddr[ADC_NUMBER_OF_GROUPS];

/** \brief  Contains a pointer to the selected ADC driver post-build groups configuration structure. */
static Adc_GroupConfigType * Adc_pt_GroupsConfig;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Sets up the ADC hardware units in regular conversion mode and loads the groups control configuration for
 *             runtime use.
 * \param      ConfigPtr : Pointer to the post-build configuration data to be loaded.
 * \return     -
 */
void Adc_Init(const Adc_ConfigType* ConfigPtr)
{
   /* Initialize all the ADC control registers. */
   RegInit_gv_Masked32Bits(ConfigPtr->pt_InitConfig);

   /* Load the groups control configuration. */
   Adc_pt_GroupsConfig = (Adc_GroupConfigType *) ConfigPtr->pt_GroupsConfig;
}

/**
 * \brief      Saves the result buffer address of the specified ADC group for storing the conversion result. In this
 *             implementation the result buffer address has to be provided before each group conversion and the status
 *             of the conversion is continuously polled until it's ready.
 * \param      Group : ID of a specific ADC group.
 * \param      DataBufferPtr : Pointer to result buffer.
 * \return     Always E_OK. No defensive mechanisms are implemented.
 */
Std_ReturnType Adc_SetupResultBuffer(Adc_GroupType Group, const Adc_ValueGroupType* DataBufferPtr)
{
   /* Save the result buffer address of the specified ADC group. */
   Adc_apt_ResultBuffersAddr[Group] = (Adc_ValueGroupType*) DataBufferPtr;
   return E_OK;
}

/**
 * \brief      Starts the conversion of the specified ADC group.
 * \param      Group : ID of a specific ADC group.
 * \return     -
 */
void Adc_StartGroupConversion(Adc_GroupType Group)
{
   Adc_GroupConfigType t_GroupConfig;

   /* Read the group configuration to avoid multiple indexing. */
   t_GroupConfig = Adc_pt_GroupsConfig[Group];

   /* Set the first (and only) ADC hardware channel to be used in the regular sequence conversion. */
   t_GroupConfig.pt_HardwareUnit->SQR3 = t_GroupConfig.ul_HardwareChannelId;

   /* Start the conversion. */
   t_GroupConfig.pt_HardwareUnit->CR2 |= ADC_CR2_SWSTART;
}

/**
 * \brief      Returns the conversion status of the specified ADC group.
 * \param      Group : ID of a specific ADC group.
 * \return     Current conversion status for the specified group.
 */
Adc_StatusType Adc_GetGroupStatus(Adc_GroupType Group)
{
   Adc_StatusType t_ReturnValue;
   Adc_GroupConfigType t_GroupConfig;
   uint32 ul_Status;

   /* Read the group configuration to avoid multiple indexing. */
   t_GroupConfig = Adc_pt_GroupsConfig[Group];

   /* Read the status register. */
   ul_Status = t_GroupConfig.pt_HardwareUnit->SR;

   /* Check if the conversion finished. */
   if ((ul_Status & ADC_SR_EOC) != 0UL)
   {
      /* Conversion finished. Store the conversion result to the result buffer. */
      t_ReturnValue = ADC_COMPLETED;
      *Adc_apt_ResultBuffersAddr[Group] = t_GroupConfig.pt_HardwareUnit->DR;
   }
   else
   {
      /* Conversion not yet finished. */
      t_ReturnValue = ADC_BUSY;
   }
   return t_ReturnValue;
}
