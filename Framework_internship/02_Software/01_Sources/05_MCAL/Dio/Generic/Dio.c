/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements the AUTOSAR DIO interfaces for controlling all the individual channels, channel groups and
 *                port instances for both input and output flows.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Dio.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == DIO_CHANNELS_API)
/**
 * \brief		Reads the current logical value from the specified individual input channel.
 * \param      ChannelId : ID of a specific individual input channel.
 * \return	   Current logical value (STD_HIGH or STD_LOW) on the specified individual input channel.
 */
Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId)
{
   Dio_LevelType t_ReturnValue;
   Dio_ChannelCfgType t_ChannelCfg;

   /* Read the channel configuration to avoid multiple indexing. */
   t_ChannelCfg = Dio_gkat_Channels[ChannelId];

   /* Get the current value from the input channel. */
   if (((uint16) Dio_gkat_PortAdresses[t_ChannelCfg.t_PortId].t_Registers->IDR & t_ChannelCfg.us_SelectMask) != 0U)
   {
      t_ReturnValue = (Dio_LevelType) STD_HIGH;
   }
   else
   {
      t_ReturnValue = (Dio_LevelType) STD_LOW;
   }

   return t_ReturnValue;
}

/**
 * \brief      Writes the specified individual output channel with the specified logical value.
 * \param      ChannelId : ID of a specific individual output channel.
 * \param      Level : Logical value (STD_HIGH or STD_LOW) to be applied on the specified individual output channel.
 * \return     -
 */
void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level)
{
   Dio_ChannelCfgType t_ChannelCfg;

   /* Read the channel configuration to avoid multiple indexing. */
   t_ChannelCfg = Dio_gkat_Channels[ChannelId];

   /* Apply the desired level on the output channel. */
   if ((Dio_ChannelType) STD_HIGH == Level)
   {
      Dio_gkat_PortAdresses[t_ChannelCfg.t_PortId].t_Registers->ODR |= t_ChannelCfg.us_SelectMask;
   }
   else /* (Dio_ChannelType) STD_LOW == Level) */
   {
      Dio_gkat_PortAdresses[t_ChannelCfg.t_PortId].t_Registers->ODR &= ~(t_ChannelCfg.us_SelectMask);
   }
}

/**
 * \brief      Toggles and returns the current logical value on a specified individual output channel.
 * \param      ChannelId : ID of a specific individual output channel.
 * \return     Current, after toggle, logical value (STD_HIGH or STD_LOW) on the specified individual output channel.
 */
Dio_LevelType Dio_FlipChannel(Dio_ChannelType ChannelId)
{
   Dio_LevelType t_ReturnValue;
   Dio_ChannelCfgType t_ChannelCfg;

   /* Read the channel configuration to avoid multiple indexing. */
   t_ChannelCfg = Dio_gkat_Channels[ChannelId];

   /* Toggle the output channel with the configured mask. */
   Dio_gkat_PortAdresses[t_ChannelCfg.t_PortId].t_Registers->ODR ^= t_ChannelCfg.us_SelectMask;

   /* Get the current value, after the toggle, from the output channel. */
   if (((uint16) Dio_gkat_PortAdresses[t_ChannelCfg.t_PortId].t_Registers->ODR & t_ChannelCfg.us_SelectMask) != 0U)
   {
      t_ReturnValue = (Dio_LevelType) STD_HIGH;
   }
   else
   {
      t_ReturnValue = (Dio_LevelType) STD_LOW;
   }

   return t_ReturnValue;
}
#endif

/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == DIO_CHANNEL_GROUPS_API)
/**
 * \brief      Reads the current logical values (atomic read) of all the channels from the specified channel group.
 * \param      ChannelGroupIdPtr : ID of a specific channel group as an address to the configuration table.
 * \return     Current logical values (masked and shifted to the LSB) of all the channels from the channel group.
 */
Dio_PortLevelType Dio_ReadChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr)
{
   Dio_PortLevelType t_InputPortValue;

   /* Get the entire value from the read port that contains the channel group. */
   t_InputPortValue = (Dio_PortLevelType) Dio_gkat_PortAdresses[ChannelGroupIdPtr->Port].t_Registers->IDR;

   /* Select only the channels from the channel group. */
   t_InputPortValue &= (Dio_PortLevelType) ChannelGroupIdPtr->Mask;

   /* Shift the channel group to the LSB position. */
   t_InputPortValue >>= ChannelGroupIdPtr->Offset;

   return t_InputPortValue;
}

/**
 * \brief      Writes the desired logical values (atomic write) of all the channels from the specified channel group.
 * \param      ChannelGroupIdPtr : ID of a specific channel group as an address to the configuration table.
 * \param      Level : Logical values to be applied on the specified channel group.
 * \return     -
 *
 * This function assumes that the caller provides a level value in the bounds of the channel group. If the level value
 * spreads over more bits than the length of the channel group, then additional bits, out of the channel groups, are
 * modified.
 */
void Dio_WriteChannelGroup(const Dio_ChannelGroupType* ChannelGroupIdPtr, Dio_PortLevelType Level)
{
   Dio_PortLevelType t_OutputPortValue;

   /* Get the entire value from the output port that contains the channel group. */
   t_OutputPortValue = (Dio_PortLevelType) Dio_gkat_PortAdresses[ChannelGroupIdPtr->Port].t_Registers->ODR;

   /* Reset the bits from the channel group. */
   t_OutputPortValue &= (Dio_PortLevelType) ~ChannelGroupIdPtr->Mask;

   /* Set the bits from the channel group with the desired level. */
   t_OutputPortValue |= (Dio_PortLevelType) (Level << ChannelGroupIdPtr->Offset);

   /* Write back the entire value on the output port containing the modified channel group. */
   Dio_gkat_PortAdresses[ChannelGroupIdPtr->Port].t_Registers->ODR = (uint32) t_OutputPortValue;
}
#endif

/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == DIO_PORTS_API)
/**
 * \brief      Gets the full value from the specified input port.
 * \param      PortId : ID of a specific input port.
 * \return     Full value from the specified input port.
 */
Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId)
{
   return (Dio_PortLevelType) Dio_gkat_PortAdresses[PortId].t_Registers->IDR;
}

/**
 * \brief      Sets the desired full value on the specified output port.
 * \param      PortId : ID of a specific output port.
 * \param      Level : Value to be applied on the output port.
 * \return     -
 */
void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level)
{
   Dio_gkat_PortAdresses[PortId].t_Registers->ODR = (Dio_PortLevelType) Level;
}
#endif
