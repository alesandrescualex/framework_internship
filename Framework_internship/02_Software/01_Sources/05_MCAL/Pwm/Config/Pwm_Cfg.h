/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm_Cfg.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Defines and exports the AUTOSAR PWM number of configured channels and the ID of each channel.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef PWM_CFG_H
#define PWM_CFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------- API Activation Macros ----------------------------------------------*/

/** \brief  Defines if the set duty cycle API is activated or not. */
#define PWM_SET_DUTY_CYCLE_API               (STD_ON)

/** \brief  Defines if the set duty cycle and period API is activated or not. */
#define PWM_SET_PERIOD_AND_DUTY_API          (STD_OFF)

/** \brief  Defines if the set output to idle API is activated or not. */
#define PWM_SET_OUTPUT_TO_IDLE_API           (STD_OFF)

/** \brief  Defines if the get output state API is activated or not. */
#define PWM_GET_OUTPUT_STATE_API             (STD_OFF)

/*---------------------------------------------- Implementation Macros ----------------------------------------------*/

/** \brief  Defines how many PWM channels are used in the configuration. */
#define PWM_NUMBER_OF_CHANNELS               (4U)

/*--------------------------------------------------- Channel IDs ---------------------------------------------------*/

/** \brief  Defines the ID of the port C pin 6 channel. */
#define PWM_CHANNEL_C_6                      (0U)

/** \brief  Defines the ID of the port C pin 7 channel. */
#define PWM_CHANNEL_C_7                      (1U)

/** \brief  Defines the ID of the port C pin 8 channel. */
#define PWM_CHANNEL_C_8                      (2U)

/** \brief  Defines the ID of the port C pin 9 channel. */
#define PWM_CHANNEL_C_9                      (3U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* PWM_CFG_H */
