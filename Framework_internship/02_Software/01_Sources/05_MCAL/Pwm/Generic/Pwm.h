/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Declares the AUTOSAR PWM standard and implementation data types, exports the global post build
 *                configurations and exports the interfaces for controlling all the channels.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef PWM_H
#define PWM_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Pwm_Cfg.h"

#include "RegInit.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#if ((STD_OFF == PWM_SET_DUTY_CYCLE_API) && (STD_OFF == PWM_SET_PERIOD_AND_DUTY_API))
#error "Invalid PWM driver configuration. At least one duty cycle control API (PWM_SET_DUTY_CYCLE_API or \
PWM_SET_PERIOD_AND_DUTY_API) shall be activated."
#endif

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------- AUTOSAR Types --------------------------------------------------*/

/** \brief  Represents the type that holds the numeric IDs of all PWM channels. The IDs are zero based. */
typedef uint8 Pwm_ChannelType;

/** \brief  Represents the type that defines the period of a PWM channel. */
typedef uint32 Pwm_PeriodType;

/** \brief  Represents the type that defines the output state of a PWM channel. */
typedef enum
{
   /** \brief  The PWM channel is in low state. */
   PWM_LOW,

   /** \brief  The PWM channel is in high state. */
   PWM_HIGH,
} Pwm_OutputStateType;

/*---------------------------------------------- Implementation Types -----------------------------------------------*/

/** \brief  Represents the configuration of a PWM channel, consisting of the duty cycle control register address and
 *          the period control register address. */
typedef struct
{
   /** \brief  Contains the address of the duty cycle control register. */
   volatile uint32* pul_DutyCycleReg;

   /** \brief  Contains the address of the period control register. */
   volatile uint32* pul_PeriodReg;

#if (STD_ON == PWM_SET_OUTPUT_TO_IDLE_API)
   /** \brief  Contains the polarity (the value from the beginning of the cycle) of the PWM channel. Correlated with
    *          CCxP bit from the CCER register. */
   Pwm_OutputStateType t_Polarity;

   /** \brief  Contains the output idle state to be set with the Pwm_SetOutputToIdle API. */
   Pwm_OutputStateType t_OutputIdleState;
#endif

#if (STD_ON == PWM_GET_OUTPUT_STATE_API)
   /** \brief  Contains the address of the input data register that can read the current value from the PWM channel. */
   volatile uint32* pul_InputDataReg;

   /** \brief  Contains the pin mask of the input data register that the PWM channel is routed to. */
   uint16 us_InputDataMask;
#endif
} Pwm_ChannelCtrlType;

/*---------------------------------------- AUTOSAR Global Configuration Type ----------------------------------------*/

/** \brief  Represents the global PWM configuration type, consisting of a reference to the initialization configuration
 *          and a reference to the configuration of the PWM channels. */
typedef struct
{
   /** \brief  Contains the address of the configuration that is loaded in the initialization function. */
   const RegInit_Masked32BitsConfigType * pt_InitConfig;

   /** \brief  Contains the configurations of the PWM channels control registers. */
   const Pwm_ChannelCtrlType * pt_ChannelsCtrl;
} Pwm_ConfigType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Pwm_ConfigType Pwm_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Pwm_Init(const Pwm_ConfigType* ConfigPtr);

#if (STD_ON == PWM_SET_DUTY_CYCLE_API)
extern void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber, uint16 DutyCycle);
#endif

#if (STD_ON == PWM_SET_PERIOD_AND_DUTY_API)
extern void Pwm_SetPeriodAndDuty(Pwm_ChannelType ChannelNumber, Pwm_PeriodType Period, uint16 DutyCycle);
#endif

#if (STD_ON == PWM_SET_OUTPUT_TO_IDLE_API)
extern void Pwm_SetOutputToIdle(Pwm_ChannelType ChannelNumber);
#endif

#if (STD_ON == PWM_GET_OUTPUT_STATE_API)
extern Pwm_OutputStateType Pwm_GetOutputState(Pwm_ChannelType ChannelNumber);
#endif

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* PWM_H */
