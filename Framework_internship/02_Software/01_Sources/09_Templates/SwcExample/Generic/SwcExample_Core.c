//*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       SwcExample_Core.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements the behavior of the software component through generic runnables. The behavior can support
 *                multiple instances and the interaction with the RTE is performed through set or get functions.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "SwcExample.h"
#include "SwcExample_Core.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the output values after processing the input values that can be read through the get function. */
static SwcExample_Type SwcExample_at_OutputValues[SWCEXAMPLE_NUMBER_OF_INSTANCES];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes all the core global variables.
 * \param      -
 * \return     -
 */
void SwcExample_Init(void)
{
   uint8 uc_I;

   for (uc_I = 0U; uc_I < SWCEXAMPLE_NUMBER_OF_INSTANCES; uc_I++)
   {
      SwcExample_at_OutputValues[uc_I] = SWCEXAMPLE_INACTIVE;
   }
}

/**
 * \brief      Implements the cyclic digital input signals processing according to configured active values.
 * \param      -
 * \return     -
 */
void SwcExample_MainFunction(void)
{
   uint8 uc_I;
   IoHwAb_DigitalLevelType at_InputValues[SWCEXAMPLE_NUMBER_OF_INSTANCES];

   /* Read the digital input signals from IoHwAb. */
   for (uc_I = 0U; uc_I < SWCEXAMPLE_NUMBER_OF_INSTANCES; uc_I++)
   {
      at_InputValues[uc_I] = IoHwAb_DigitalGetChannel(SwcExample_gkat_IoHwAbChannels[uc_I]);
   }

   /* Set the output values according the configured active values. */
   for (uc_I = 0U; uc_I < SWCEXAMPLE_NUMBER_OF_INSTANCES; uc_I++)
   {
      if (at_InputValues[uc_I] == SwcExample_gkat_ActiveValues[uc_I])
      {
         SwcExample_at_OutputValues[uc_I] = SWCEXAMPLE_ACTIVE;
      }
      else
      {
         SwcExample_at_OutputValues[uc_I] = SWCEXAMPLE_INACTIVE;
      }
   }
}

/**
 * \brief      Returns the current output state of the specified instance.
 * \param      uc_Id : specific ID of a instance.
 * \return     The current output value of the specified instance.
 */
SwcExample_Type SwcExample_GetState(uint8 uc_Id)
{
   return SwcExample_at_OutputValues[uc_Id];
}
